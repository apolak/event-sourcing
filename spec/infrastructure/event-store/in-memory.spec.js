import InMemoryEventStore from '../../../src/infrastructure/event-store/in-memory';
import Events from '../../../src/domain/events';
import EventBus from '../../../src/infrastructure/event-bus';

import Upgraders from '../../../src/infrastructure/upgraders';
import AddPositionEventV1ToV2Upgrader from '../../../src/infrastructure/upgraders/add-position-event-v1-to-v2.upgrader';
import AddPositionEventV2ToV3Upgrader from '../../../src/infrastructure/upgraders/add-position-event-v2-to-v3.upgrader';

import uuidV4 from 'uuid/v4';

import { expect } from 'chai';

describe('In memory event store', () => {
	'use strict';

	it('it returns all events for aggregate in latest version', () => {
		const upgraders = new Upgraders();
		upgraders.addUpgrader(Events.v1.AddPositionEvent.eventName, new AddPositionEventV1ToV2Upgrader());
		upgraders.addUpgrader(Events.v2.AddPositionEvent.eventName, new AddPositionEventV2ToV3Upgrader());

		const eventBus = new EventBus();
		const eventStore = new InMemoryEventStore(eventBus, upgraders);

		const orderId = uuidV4();
		const userId = uuidV4();
		const positionV1Id = uuidV4();
		const positionV2Id = uuidV4();

		eventStore.push([
			new Events.v1.AddPositionEvent.Instance(
				orderId,
				positionV1Id,
				'Product 1',
				100
			),
			new Events.v2.AddPositionEvent.Instance(
				orderId,
				positionV2Id,
				userId,
				'Product 2',
				500
			)
		]);

		const eventsHistory = eventStore.getEventsFor(orderId);
		expect(eventsHistory.events.length).to.equal(2);

		expect(eventsHistory.events[0].id).to.equal(orderId);
		expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(eventsHistory.events[0].payload.owner)).to.be.true;
		expect(eventsHistory.events[0].payload.id).to.equal(positionV1Id);
		expect(eventsHistory.events[0].payload.name).to.equal('Product 1');
		expect(eventsHistory.events[0].payload.price).to.equal(100);
		expect(eventsHistory.events[0].payload.quantity).to.equal(1);
		expect(eventsHistory.events[0].name).to.equal(Events.v3.AddPositionEvent.eventName);
		expect(eventsHistory.events[0].version).to.equal(3);

		expect(eventsHistory.events[1].id).to.equal(orderId);
		expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(eventsHistory.events[1].payload.owner)).to.be.true;
		expect(eventsHistory.events[1].payload.id).to.equal(positionV2Id);
		expect(eventsHistory.events[1].payload.name).to.equal('Product 2');
		expect(eventsHistory.events[1].payload.price).to.equal(500);
		expect(eventsHistory.events[1].payload.quantity).to.equal(1);
		expect(eventsHistory.events[1].name).to.equal(Events.v3.AddPositionEvent.eventName);
		expect(eventsHistory.events[1].version).to.equal(3);
	});
});
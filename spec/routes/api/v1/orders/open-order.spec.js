import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import UserId from '../../../../../src/domain/user-id';

import { expect } from 'chai';

describe('API', () => {
	'use strict';

	it('Get /api/orders/:id', (done) => {
		const appConfig = config();

		request(app(appConfig).listen()).post('/api/orders')
			.expect(200)
			.end((err, res) => {
				expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(res.body.orderId)).to.be.true;
				expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(res.headers['user-id'])).to.be.true;

				const orderEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.orderId);

				expect(orderEventsHistory.events.length).to.equal(2);

				done();
			});
	});

	it('POST /api/orders with user id', (done) => {
		const userId = UserId.generate();
		const appConfig = config();

		request(app(appConfig).listen()).post('/api/orders')
			.set('user-id', userId.id)
			.expect(200)
			.end((err, res) => {
				expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(res.body.orderId)).to.be.true;
				expect(res.headers['user-id']).to.equal(userId.id);

				const orderEventsHistory = appConfig.container.services.eventStore.getEventsFor(res.body.orderId);

				expect(orderEventsHistory.events.length).to.equal(2);

				done();
			});
	});
});
import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import OrderId from '../../../../../src/domain/order/order-id';
import UserId from '../../../../../src/domain/user-id';
import Events from '../../../../../src/domain/events';
import Position from '../../../../../src/domain/order/position';
import uuidV4 from 'uuid/v4';

import { expect } from 'chai';

describe('API', () => {
	'use strict';

	it('GET /api/orders/:id', (done) => {
		const orderId = OrderId.generate();
		const userId = UserId.generate();
		const positionId = uuidV4();
		const appConfig = config();

		appConfig.container.services.eventStore.push([new Events.v1.OrderOpenedEvent.Instance(orderId.id)]);
		appConfig.container.services.eventStore.push([new Events.v1.AddOwnerEvent.Instance(orderId.id, userId)]);

		appConfig.container.services.eventStore.push([
			new Events.v3.AddPositionEvent.Instance(
				orderId.id,
				positionId,
				userId.id,
				'Product 1',
				5,
				100
			)
		]);

		request(app(appConfig).listen()).get(`/api/orders/${orderId.id}`)
			.set('user-id', userId.id)
			.expect(200)
			.end((err, res) => {
				expect(res.body.orderId).to.equal(orderId.id);
				expect(res.body.isAdmin).to.be.true;
				expect(res.body.positions.length).to.equal(1);

				expect(res.body.positions[0].owner).to.equal(userId.id);
				expect(res.body.positions[0].id).to.equal(positionId);
				expect(res.body.positions[0].name).to.equal('Product 1');
				expect(res.body.positions[0].quantity).to.equal(5);
				expect(res.body.positions[0].price).to.equal(100);
				expect(res.body.positions[0].total).to.equal(500);

				expect(res.body.prices.total).to.equal(500);

				done();
			});
	});
});

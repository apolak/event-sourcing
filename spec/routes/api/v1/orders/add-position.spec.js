import request from 'supertest';
import app from '../../../../../src/app';
import config from '../../../../../config/test.config';
import OrderId from '../../../../../src/domain/order/order-id';
import UserId from '../../../../../src/domain/user-id';
import Events from '../../../../../src/domain/events';

import { expect } from 'chai';

describe('API', () => {
	'use strict';

	it('POST /api/orders/:id/position', (done) => {
		const orderId = OrderId.generate();
		const userId = UserId.generate();
		const appConfig = config();

		appConfig.container.services.eventStore.push([new Events.v1.OrderOpenedEvent.Instance(orderId.id)]);
		appConfig.container.services.eventStore.push([new Events.v1.AddOwnerEvent.Instance(orderId.id, userId)]);

		request(app(appConfig).listen()).post(`/api/orders/${orderId.id}/positions`)
			.send({
				name: 'Product 1',
				quantity: 100,
				price: 200
			})
			.set('user-id', userId.id)
			.expect(200)
			.end((err, res) => {
				expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(res.body.id)).to.be.true;
				expect(res.body.owner).to.equal(userId.id);
				expect(res.body.name).to.equal('Product 1');
				expect(res.body.quantity).to.equal(100);
				expect(res.body.price).to.equal(200);

				const orderEventsHistory = appConfig.container.services.eventStore.getEventsFor(orderId.id);

				expect(orderEventsHistory.events.length).to.equal(3);

				done();
			});
	});
});

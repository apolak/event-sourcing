import { expect } from 'chai';
import uuidV4 from 'uuid/v4';
import OrderId from '../../../src/domain/order/order-id';

describe('Order Id', () => {
	'use strict';

	it('generates order id', () => {
		const orderId: OrderId = OrderId.generate();

		expect(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(orderId.id)).to.be.true;
	});

	it('creates order id from string', () => {
		const expectedId: string = uuidV4();

		const orderId: OrderId = OrderId.fromString(expectedId);

		expect(orderId.id).to.eql(expectedId);
	});

	it('it throws error when invalid id given', () => {
		const invalidId: string = 'invalidId';

		expect(() => OrderId.fromString(expectedId)).to.throw(Error);
	});
});
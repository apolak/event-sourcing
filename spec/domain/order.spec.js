import Order from '../../src/domain/order';
import OrderId from '../../src/domain/order/order-id';
import UserId from '../../src/domain/user-id';
import Events from '../../src/domain/events';
import EventsHistory from '../../src/domain/events-history';

import { expect } from 'chai';

describe('Order', () => {
	'use strict';

	it('opens new order', () => {
		const order = new Order(OrderId.generate());

		order.open(UserId.generate());

		expect(order.positions.length).to.equal(0);

		expect(order.status).to.equal('open');
	});

	it('recreates order from events', () => {
		const orderId = OrderId.generate();

		const events = [
			new Events.v1.OrderOpenedEvent.Instance(orderId.id)
		];

		const order = Order.reconstituteFrom(new EventsHistory(orderId, events));

		expect(order.orderId.id).to.equal(orderId.id);
	})
});
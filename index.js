import createApp from './src/app';
import config from './config/prod.config';

const app = createApp(config());
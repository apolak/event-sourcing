import UserId from '../domain/user-id';

const userIdMiddleware = (req, res, next) => {
	const userId = req.headers['user-id'] ? UserId.fromString(req.headers['user-id']) : UserId.generate();

	req.userId = userId;

	res.header('user-id', userId.id);

	next();
};

module.exports = userIdMiddleware;
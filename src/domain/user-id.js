//@flow

import uuidV4 from 'uuid/v4';

class UserId {
	id: string;

	constructor(id: string) {
		if (!/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i.test(id)) {
			throw new Error(`ID must be valid uuidV4, "${id}" given`);
		}

		this.id = id;
	}

	static generate(): UserId {
		return new UserId(uuidV4());
	}

	static fromString(id: string): UserId {
		return new UserId(id);
	}
}

module.exports = UserId;
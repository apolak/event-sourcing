//@flow

class Event {
	id: string;
	payload: Object;
	name: string;
	version: number;

	constructor(orderId: string, payload: Object) {
		this.id = orderId;
		this.payload = payload;

		this.name = 'BaseEvent';
		this.version = 1;
	}
}

module.exports = Event;
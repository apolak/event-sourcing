//@flow

import Event from '../event';
import Order from '../order';

interface EventHandler {
	canHandle(event: Event): boolean,
	handle(event: Event, order: Order): void
}

export type { EventHandler };
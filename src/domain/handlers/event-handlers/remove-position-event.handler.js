//@flow

import Event from '../../event';
import Events from '../../events';
import Order from '../../order';
import Position from '../../order/position';

import type { EventHandler } from '../event-handler';

class RemovePositionEventHandler implements EventHandler{
	canHandle(event: Event): boolean {
		return event.name === Events.v1.RemovePositionEvent.eventName && event.version === Events.v1.RemovePositionEvent.version;
	}

	handle(event: Event, order: Order): void {
		order.positions = order.positions.filter((position: Position) => position.id !== event.payload.id);
	}
}

module.exports = new RemovePositionEventHandler();
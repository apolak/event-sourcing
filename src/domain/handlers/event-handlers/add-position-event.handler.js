//@flow

import Event from '../../event';
import Events from '../../events';
import Order from '../../order';
import Position from '../../order/position';
import UserId from '../../user-id';

import type { EventHandler } from '../event-handler';

class AddPositionEventHandler implements EventHandler{
	canHandle(event: Event): boolean {
		return event.name === Events.v3.AddPositionEvent.eventName && event.version === Events.v3.AddPositionEvent.version;
	}

	handle(event: Event, order: Order): void {
		order.positions.push(new Position(
				UserId.fromString(event.payload.owner),
				event.payload.id,
				event.payload.name,
				event.payload.quantity,
				event.payload.price
			)
		);
	}
}

module.exports = new AddPositionEventHandler();
//@flow

import Event from '../../event';
import Events from '../../events';
import UserId from '../../user-id';
import Order from '../../order';

import type { EventHandler } from '../event-handler';

class AddOwnerEventHandler implements EventHandler{
	canHandle(event: Event): boolean {
		return event.name === Events.v1.AddOwnerEvent.eventName && event.version === Events.v1.AddOwnerEvent.version;
	}

	handle(event: Event, order: Order): void {
		order.owners.push(UserId.fromString(event.payload.userId));
	}
}

module.exports = new AddOwnerEventHandler();
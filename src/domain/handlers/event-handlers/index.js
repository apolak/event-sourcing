import OrderOpenedEventHandler from './order-opened-event.handler';
import AddPositionEventHandler from './add-position-event.handler';
import RemovePositionEventHandler from './remove-position-event.handler';
import AddOwnerEventHandler from './add-owner-event.handler';

module.exports = [
	OrderOpenedEventHandler,
	AddPositionEventHandler,
	RemovePositionEventHandler,
	AddOwnerEventHandler
];
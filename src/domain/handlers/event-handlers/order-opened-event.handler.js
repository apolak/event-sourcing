//@flow

import Event from '../../event';
import Events from '../../events';
import Order from '../../order';

import type { EventHandler } from '../event-handler';

class OrderOpenedEventHandler implements EventHandler{
	canHandle(event: Event): boolean {
		return event.name === Events.v1.OrderOpenedEvent.eventName && event.version === Events.v1.OrderOpenedEvent.version;
	}

	handle(event: Event, order: Order): void {
		order.status = 'open';
	}
}

module.exports = new OrderOpenedEventHandler();
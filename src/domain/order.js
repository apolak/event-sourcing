//@flow
import uuidV4 from 'uuid/v4';

import OrderId from './order/order-id';
import UserId from './user-id';
import EventsHistory from './events-history';
import Event from './event';

import eventHandlers from './handlers/event-handlers';
import type { EventHandler } from './handlers/event-handler';

import Events from './events';
import Position from './order/position';

class Order {
	owners: Array<UserId>;
	orderId: OrderId;
	positions: Array<Position>;
	status: string;

	recentEvents: Array<Event>;

	constructor(orderId: OrderId) {
		this.orderId = orderId;
		this.positions = [];
		this.recentEvents = [];
		this.owners = [];
		this.status = '';
	}

	static reconstituteFrom(eventsHistory: EventsHistory) {
		let order = new Order(eventsHistory.orderId);

		eventsHistory.events.forEach((event: Event) => order.applyEvent(event));

		return order;
	}

	open(userId: UserId) {
		const openEvent = new Events.v1.OrderOpenedEvent.Instance(this.orderId.id);
		const addOwnerEvent = new Events.v1.AddOwnerEvent.Instance(this.orderId.id, userId);

		this.recentEvents.push(openEvent);
		this.recentEvents.push(addOwnerEvent);

		this.applyEvent(openEvent);
		this.applyEvent(addOwnerEvent);
	}

	addPosition(userId: UserId, position: Object) {
		const event = new Events.v3.AddPositionEvent.Instance(this.orderId.id, uuidV4(), userId.id, position.name, position.quantity, position.price);

		this.recentEvents.push(event);

		this.applyEvent(event);
	}

	removePosition(userId: UserId, positionId: string) {
		const event = new Events.v1.RemovePositionEvent.Instance(this.orderId.id, positionId);

		this.recentEvents.push(event);

		this.applyEvent(event);
	}

	applyEvent(event: Event) {
		const handler = eventHandlers.find((handler: EventHandler) => handler.canHandle(event));

		if (handler === undefined) {
			throw new Error(`Could not find handler for event ${event.name}`);
		}

		handler.handle(event, this);
	}

	getRecentEvents() {
		const recentEvents = this.recentEvents;

		this.recentEvents = [];

		return recentEvents;
	}
}

module.exports = Order;
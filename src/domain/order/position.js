//@flow

import UserId from '../user-id';

class Position {
	owner: UserId;
	id: string;
	name: string;
	quantity: number;
	price: number;

	constructor(owner: UserId, id: string, name: string, quantity: number, price: number) {
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.owner = owner;
	}
}

module.exports = Position;
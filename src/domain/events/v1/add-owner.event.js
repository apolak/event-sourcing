//@flow

import Event from '../../event';
import UserId from '../../user-id';

const NAME = 'AddOwnerEvent';
const VERSION = 1;

class AddOwnerEvent extends Event {
	constructor(orderId: string, userId: UserId) {
		super(orderId, {
			userId: userId.id
		});

		this.name = NAME;
		this.version = VERSION;
	}

	static fromPayload(orderId: string, payload: Object) {
		return new AddOwnerEvent(orderId, UserId.fromString(payload.userId));
	}
}

module.exports.Instance = AddOwnerEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
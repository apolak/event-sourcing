//@flow

import Event from '../../event';

const NAME = 'OrderOpenedEvent';
const VERSION = 1;

class OrderOpenedEvent extends Event {
	constructor(orderId: string) {
		super(orderId, {});

		this.name = NAME;
		this.version = VERSION;
	}

	static fromPayload(orderId: string, payload: Object) {
		return new OrderOpenedEvent(orderId);
	}
}

module.exports.Instance = OrderOpenedEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
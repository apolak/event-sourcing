//@flow

import Event from '../../event';
import Position from '../../order/position';

const NAME = 'RemovePositionEvent';
const VERSION = 1;

class RemovePositionEvent extends Event {
	constructor(orderId: string, positionId: string) {
		super(orderId, {
			id: positionId
		});

		this.name = NAME;
		this.version = VERSION;
	}

	static fromPayload(orderId: string, payload: Object) {
		return new RemovePositionEvent(orderId, payload.id);
	}
}

module.exports.Instance = RemovePositionEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
//@flow

import Event from '../../event';
import Position from '../../order/position';

const NAME = 'AddPositionEvent';
const VERSION = 1;

class AddPositionEvent extends Event {
	constructor(orderId: string, positionId: string, name: string, price: number) {
		super(orderId, {
			id: positionId,
			name,
			price
		});

		this.name = NAME;
		this.version = VERSION;
	}

	static fromPayload(orderId: string, payload: Object) {
		return new AddPositionEvent(orderId, payload.id, payload.name, payload.price);
	}
}

module.exports.Instance = AddPositionEvent;
module.exports.eventName = NAME;
module.exports.version = VERSION;
import * as OrderOpenedEvent from './v1/order-opened.event';
import * as AddPositionEventV1 from './v1/add-position.event';
import * as AddPositionEventV2 from './v2/add-position.event';
import * as AddPositionEventV3 from './v3/add-position.event';
import * as RemovePositionEvent from './v1/remove-position.event';
import * as AddOwnerEvent from './v1/add-owner.event';

module.exports = {
	v1: {
		OrderOpenedEvent: OrderOpenedEvent,
		AddPositionEvent: AddPositionEventV1,
		RemovePositionEvent: RemovePositionEvent,
		AddOwnerEvent: AddOwnerEvent
	},
	v2: {
		AddPositionEvent: AddPositionEventV2
	},
	v3: {
		AddPositionEvent: AddPositionEventV3
	}
};
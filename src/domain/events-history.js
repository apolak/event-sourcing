//@flow

import OrderId from './order/order-id';

class EventsHistory {
	orderId: OrderId;
	events: Array<Object>;

	constructor(orderId: OrderId, events: Array<Object>) {
		this.orderId = orderId;
		this.events = events;
	}
}

module.exports = EventsHistory;
//@flow

import UserId from '../../domain/user-id';

type Users = {
	[key: string]: Array<string>
};

class UserOrdersInMemoryRepository {
	users: Users;

	constructor() {
		this.users = {};
	}

	addOrder(userId: string, orderId: string) {
		if (this.users[userId] === undefined) {
			this.users[userId] = [orderId];
		} else {
			this.users[userId].push(orderId);
		}
	}

	findUserOrders(userId: UserId): Array<string>|null {
		if (this.users[userId.id] === undefined) {
			return null;
		}

		return this.users[userId.id];
	}
}

module.exports = UserOrdersInMemoryRepository;
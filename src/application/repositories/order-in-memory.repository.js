//@flow

import OrderId from '../../domain/order/order-id';

import type { Order } from '../model/order';
import type { Position } from '../model/position';

type Orders = {
	[key: string]: Order
};

class OrderInMemoryRepository {
	orders: Orders;

	constructor() {
		this.orders = {};
	}

	add(orderId: string) {
		this.orders[orderId] = {
			id: orderId,
			total: 0,
			positions: []
		};
	}

	addPosition(orderId: string, position: Position) {
		this.orders[orderId].positions.push(position);
		this.orders[orderId].total += position.quantity * position.price;
	}

	removePosition(orderId: string, positionIdToBeRemoved: string) {
		const position = this.orders[orderId].positions.find((position) => position.id === positionIdToBeRemoved);

		if (position) {
			this.orders[orderId].positions = this.orders[orderId].positions.filter((position) => position.id !== positionIdToBeRemoved);
			this.orders[orderId].total -= position.total;
		}
	}

	findOrder(orderId: OrderId): Order|null {
		if (this.orders[orderId.id] === undefined) {
			return null;
		}

		return this.orders[orderId.id];
	}
}

module.exports = OrderInMemoryRepository;
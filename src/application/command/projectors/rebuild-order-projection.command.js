import config from '../../../../config/prod.config';

const configuration = config();

const events = configuration.container.services.eventStore.getAll();

events.forEach((event) => configuration.container.projectors.orderProjector.handle(event));
//@flow

export type Position = {
	id: string,
	owner: string,
	name: string,
	quantity: number,
	total: number,
	price: number
};
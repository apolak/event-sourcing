//@flow

import type { Position } from './position';

export type Order = {
	id: string,
	total: number,
	positions: Array<Position>
}

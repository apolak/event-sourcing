//@flow

import Event from '../../domain/event';
import Events from '../../domain/events';
import UserOrderInMemoryRepository from '../repositories/user-orders-in-memory.repository';
import type { Subscriber } from '../../infrastructure/event-bus/subscriber';

class UserOrdersInMemoryProjector implements Subscriber {
	userOrdersRepository: UserOrderInMemoryRepository;

	constructor(userOrdersRepository: UserOrderInMemoryRepository) {
		this.userOrdersRepository = userOrdersRepository;
	}

	handle(event: Event): void {
		if (event.name === Events.v1.AddOwnerEvent.eventName && event.version === Events.v1.AddOwnerEvent.version) {
			this.userOrdersRepository.addOrder(event.payload.userId, event.id);
		}
	}
}

module.exports = UserOrdersInMemoryProjector;
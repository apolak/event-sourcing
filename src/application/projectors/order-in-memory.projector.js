//@flow

import Event from '../../domain/event';
import Events from '../../domain/events';
import type { Subscriber } from '../../infrastructure/event-bus/subscriber';
import OrderRepository from '../repositories/order-in-memory.repository';

class OrderInMemoryProjector implements Subscriber {
	orderRepository: OrderRepository;

	constructor(orderRepository: OrderRepository) {
		this.orderRepository = orderRepository;
	}

	handle(event: Event): void {
		if (event.name === Events.v1.OrderOpenedEvent.eventName && event.version === Events.v1.OrderOpenedEvent.version) {
			this.orderRepository.add(event.id);
		}

		if (event.name === Events.v3.AddPositionEvent.eventName && event.version === Events.v3.AddPositionEvent.version) {
			this.orderRepository.addPosition(event.id, {
				id: event.payload.id,
				owner: event.payload.owner,
				name: event.payload.name,
				price: event.payload.price,
				quantity: event.payload.quantity,
				total: event.payload.price * event.payload.quantity
			});
		}

		if (event.name === Events.v1.RemovePositionEvent.eventName && event.version === Events.v1.RemovePositionEvent.version) {
			this.orderRepository.removePosition(event.id, event.payload.id);
		}
	}
}

module.exports = OrderInMemoryProjector;
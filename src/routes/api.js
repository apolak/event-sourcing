import express from 'express';

module.exports = (container) => {
	const router = express.Router();

	router.use('/', require('./api/v1.js')(container));

	return router;
};

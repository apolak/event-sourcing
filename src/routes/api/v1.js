import express from 'express';

module.exports = (container) => {
	const router = express.Router();

	router.use('/orders', require('./v1/orders.js')(container));

	return router;
};

import express from 'express';

module.exports = (container) => {
	const router = express.Router();

	router.post('/', require ('./orders/open-order.js')(container));
	router.get('/:id', require ('./orders/get-order.js')(container));
	router.post('/:id/positions', require ('./orders/add-position.js')(container));
	router.delete('/:id/positions/:positionId', require ('./orders/remove-position.js')(container));

	return router;
};

'use strict';

import OrderId from '../../../../../src/domain/order/order-id';

module.exports = (container) => (req, res) => {
	const orderId = OrderId.fromString(req.params.id);
	const userOrders = container.repositories.userOrdersRepository.findUserOrders(req.userId);

	const orderReadModel = container.repositories.orderRepository.findOrder(orderId);

	res.json({
		orderId: orderReadModel.id,
		isAdmin: userOrders.findIndex((id) => id === orderReadModel.id) >= 0,
		positions: orderReadModel.positions,
		prices: {
			total: orderReadModel.total
		}
	});
};

'use strict';

import Order from '../../../../domain/order';

module.exports = (container) => (req, res) => {
	const order = Order.reconstituteFrom(container.services.eventStore.getEventsFor(req.params.id));

	order.addPosition(req.userId, req.body);

	const recentEvents = order.getRecentEvents();

	container.services.eventStore.push(recentEvents);

	const orderPositions = container.repositories.orderRepository.findOrder(order.orderId).positions;

	res.json(orderPositions[orderPositions.length - 1]);
};

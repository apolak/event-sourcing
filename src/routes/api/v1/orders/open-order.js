'use strict';

import OrderId from '../../../../../src/domain/order/order-id';
import Order from '../../../../../src/domain/order';

module.exports = (container) => (req, res) => {
	const orderId = OrderId.generate();
	const order = new Order(orderId);

	order.open(req.userId);

	const recentEvents = order.getRecentEvents();

	container.services.eventStore.push(recentEvents);

	res.json({
		orderId: orderId.id
	});
};

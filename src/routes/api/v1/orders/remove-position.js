'use strict';

import Order from '../../../../domain/order';

module.exports = (container) => (req, res) => {
	const order = Order.reconstituteFrom(container.services.eventStore.getEventsFor(req.params.id));

	order.removePosition(req.userId, req.params.positionId);

	const recentEvents = order.getRecentEvents();

	container.services.eventStore.push(recentEvents);

	res.json({
		id: req.params.positionId
	});
};

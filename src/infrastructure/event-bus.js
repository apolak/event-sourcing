//@flow

import Event from '../domain/event';
import type { Subscriber } from './event-bus/subscriber';

class EventBus {
	subscribers: Array<Subscriber>;

	constructor() {
		this.subscribers = [];
	}

	subscribe(subscriber: Subscriber) {
		this.subscribers.push(subscriber);
	}

	publish(event: Event) {
		this.subscribers.forEach((subscriber: Subscriber) => subscriber.handle(event));
	}
}

module.exports = EventBus;
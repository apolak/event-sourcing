import Event from '../domain/event';

interface Upgrader {
	canUpgrade(from: Event): boolean;
	upgrade(from: Event): Event;
}

export type { Upgrader };
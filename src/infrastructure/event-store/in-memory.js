//@flow

import Event from '../../domain/event';
import Events from '../../domain/events';
import EventsHistory from '../../domain/events-history';
import OrderId from '../../domain/order/order-id';
import EventBus from '../event-bus';
import Upgraders from '../upgraders';

class InMemoryEventStore {
	events: Array<Object>;
	eventBus: EventBus;
	upgraders: Upgraders;

	constructor(eventBus: EventBus, upgraders: Upgraders) {
		this.events = [];
		this.eventBus = eventBus;
		this.upgraders = upgraders;
	}

	push(events: Array<Event>) {
		events.forEach((event: Event) => {
			this.events.push({
				aggregateId: event.id,
				name: event.name,
				version: event.version,
				payload: event.payload
			});

			this.eventBus.publish(event);
		});
	}

	getAll(): Array<Event> {
		return this.events
			.map((event) => Events[`v${event.version}`][event.name].Instance.fromPayload(event.aggregateId, event.payload))
			.map((event) => this.upgraders.upgrade(event));
	}

	getEventsFor(aggregateId: string): EventsHistory {
		return new EventsHistory(
			OrderId.fromString(aggregateId),
			this.events
				.filter((event) => event.aggregateId === aggregateId)
				.map((event) => Events[`v${event.version}`][event.name].Instance.fromPayload(event.aggregateId, event.payload))
				.map((event) => this.upgraders.upgrade(event))
		);
	}
}

module.exports = InMemoryEventStore;
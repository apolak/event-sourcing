//@flow

import Events from '../../domain/events';
import Event from '../../domain/event';
import type { Upgrader } from '../upgrader';

class AddPositionEventV2ToV3Upgrader implements Upgrader {
	canUpgrade(from: Event): boolean {
		return from.name === Events.v2.AddPositionEvent.eventName && from.version === Events.v2.AddPositionEvent.version;
	}

	upgrade(from: Event): Event {
		return new Events.v3.AddPositionEvent.Instance(
			from.id,
			from.payload.id,
			from.payload.owner,
			from.payload.name,
			1,
			from.payload.price
		);
	}
}

module.exports = AddPositionEventV2ToV3Upgrader;
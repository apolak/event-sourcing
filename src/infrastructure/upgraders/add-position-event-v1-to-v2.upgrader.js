//@flow

import Events from '../../domain/events';
import Event from '../../domain/event';
import type { Upgrader } from '../upgrader';

import uuidV4 from 'uuid/v4';

class AddPositionEventV1ToV2Upgrader implements Upgrader {
	canUpgrade(from: Event): boolean {
		return from.name === Events.v1.AddPositionEvent.eventName && from.version === Events.v1.AddPositionEvent.version;
	}

	upgrade(from: Event): Event {
		return new Events.v2.AddPositionEvent.Instance(
			from.id,
			from.payload.id,
			uuidV4(),
			from.payload.name,
			from.payload.price
		);
	}
}

module.exports = AddPositionEventV1ToV2Upgrader;
//@flow

import type { Upgrader } from './upgrader';
import Event from '../domain/event';

type EventUpgraders = {
	[eventName: string]: Array<Upgrader>
};

class Upgraders {
	eventUpgraders: EventUpgraders;

	constructor() {
		this.eventUpgraders = {};
	}

	addUpgrader(eventName: string, upgrader: Upgrader) {
		if (this.eventUpgraders[eventName] === undefined) {
			this.eventUpgraders[eventName] = [upgrader];
		} else {
			this.eventUpgraders[eventName].push(upgrader);
		}
	}

	upgrade(event: Event): Event {
		if (this.eventUpgraders[event.name]) {
			return this.eventUpgraders[event.name].reduce((currentEvent, upgrader) => {
				if (upgrader.canUpgrade(currentEvent)) {
					return upgrader.upgrade(currentEvent);
				}

				return currentEvent;
			}, event);
		}

		return event;
	}
}

module.exports = Upgraders;
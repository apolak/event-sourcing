import InMemoryEventStore from '../src/infrastructure/event-store/in-memory';
import EventBus from '../src/infrastructure/event-bus';
import Events from '../src/domain/events';

import UserOrdersInMemoryProjector from '../src/application/projectors/user-orders-in-memory.projector';
import OrderInMemoryProjector from '../src/application/projectors/order-in-memory.projector';

import OrderInMemoryRepository from '../src/application/repositories/order-in-memory.repository';
import UserOrdersInMemoryRepository from '../src/application/repositories/user-orders-in-memory.repository';

import Upgraders from '../src/infrastructure/upgraders';
import AddPositionEventV1ToV2Upgrader from '../src/infrastructure/upgraders/add-position-event-v1-to-v2.upgrader';
import AddPositionEventV2ToV3Upgrader from '../src/infrastructure/upgraders/add-position-event-v2-to-v3.upgrader';

module.exports = () => {
	'use strict';
	const upgraders = new Upgraders();
	upgraders.addUpgrader(Events.v1.AddPositionEvent.eventName, new AddPositionEventV1ToV2Upgrader());
	upgraders.addUpgrader(Events.v2.AddPositionEvent.eventName, new AddPositionEventV2ToV3Upgrader());

	const eventBus = new EventBus();
	const eventStore = new InMemoryEventStore(eventBus, upgraders);

	const orderRepository = new OrderInMemoryRepository();
	const userOrdersRepository = new UserOrdersInMemoryRepository();

	const userOrdersInMemoryProjector = new UserOrdersInMemoryProjector(userOrdersRepository);
	const orderInMemoryProjector = new OrderInMemoryProjector(orderRepository);

	eventBus.subscribe(userOrdersInMemoryProjector);
	eventBus.subscribe(orderInMemoryProjector);

	return {
		port: 3333,
		container: {
			services: {
				eventStore: eventStore
			},
			repositories: {
				orderRepository: orderRepository,
				userOrdersRepository: userOrdersRepository
			},
			projectors: {
				orderProjector: orderInMemoryProjector,
				userOrdersProjector: userOrdersInMemoryProjector
			}
		}
	};
};